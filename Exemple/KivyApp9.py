from kivy.app import App
from kivy.lang import Builder

#loadind kv files

presentation = Builder.load_file("main.kv")

class MainApp(App):
    def build(self):
        return DrawInput()

if __name__ == "__main__":
    MainApp().run()