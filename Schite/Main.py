from kivy.app import App
from kivy.lang import Builder
from kivy.uix.screenmanager import ScreenManager, Screen, FadeTransition

#ScreenManager: 5 screens

from kivy.core.window import Window
Window.clearcolor = (1, 1, 1, 1)

class MainScreen(Screen):
    pass

class Clasa9Screen(Screen):
    pass

class Clasa10Screen(Screen):
    pass

class Clasa11Screen(Screen):
    pass

class Clasa12Screen(Screen):
    pass


class ScreenManagement(ScreenManager):
    pass


presentation = Builder.load_file("main.kv")

class MainApp(App):
    def build(self):
        return presentation

if __name__ == "__main__":
    MainApp().run()